<html>

<head>
    <title>Amazing Not-Alien Search</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <style>
        @keyframes fadein {
            from { opacity: 0; }
            to { opacity 1; }
        }

        body {
            margin-bottom: 10em;
        }

    </style>

</head>

<body>

<div class="col-md-8 offset-md-2" style="margin-top: 5em;">
    <div class="content">
        <img class="img-fluid rounded offset-md-4 col-md-4" src="img/pepe.png" style="animation: fadein 3s;"/>

        <p class="float-right">Made by Erwin Boot, Kevin van Geemen en Hans Goor</p>
        <form method="post">
            <div class="form-group">
                <label for="seek">Search for ('*' for all results): </label>
                <input class="form-control" id="seek" name="seek" />
            </div>
            <input class="btn btn-primary float-left" id="submit" name="submit" type="submit" value="Submit" />
            <div class="float-right">
                <a class="btn btn-primary" href="/remove.php">Remove a fake human</a>
                <a class="btn btn-primary" href="/register.php">Register as a real human</a>
            </div>
        </form>
    </div>
</div>

<?php

if($_SERVER["REQUEST_METHOD"] === "POST") {
    $db_name = "humans";
    $user = "notaspy";
    $pass = "totallynotone";
    $db = new mysqli("database", $user, $pass, $db_name);

    if($db->connect_error) {
        die('<div class="col-md-8 offset-md-2"><div class="content"><p class="text-err">Could not connect to database!</p></div></div>');
    }

    $seek = $db->real_escape_string($_POST["seek"]);
    if($seek === "*") {
        $sql = 'SELECT * FROM `general_info`;';
    } else {
        $sql = 'SELECT * FROM `general_info` WHERE `name` LIKE "%'.$seek.'%" OR `surname` LIKE "%'.$seek.'%";';
    }
    $result = $db->query($sql);

    if($result->num_rows === 0) {
        die('<div class="col-md-8 offset-md-2" style="margin-top: 5em;"><div class="content text-center"><p>No results.</p></div></div>');
    }

    echo '<div class="col-md-8 offset-md-2" style="margin-top: 5em;"><div class="content">';
    echo '<table class="table"><thead><tr><th scope="col">First Name</th><th scope="col">Surname</th><th scope="col">Address</th><th scope="col">Postal Code</th><th scope="col">Date of Birth</th><th scope="col">Has Pets</th></tr></thead><tbody>';
    while($row = $result->fetch_assoc()) {
        $has_pets = ($row["has_pets"] === "1" ? "Yes" : "No");
        echo "<tr><td>".$row["name"]."</td><td>".$row["surname"]."</td><td>".$row["address"]."</td><td>".$row["postal_code"]."</td><td>".$row["date_of_birth"]."</td><td>".$has_pets."</td></tr>";
    }
    echo "</tbody></table></div></div>";

}
?>

</body>

</html>
