<html>

<head>
	<title>Mijn site</title>
</head>

<body>
<form method="post">
	<label for="seek">Zoeken naar: </label>
	<input id="seek" name="seek" type="text" />
	<input id="submit" name="submit" type="submit" value="Submit" />
</form>

<?php

if($_SERVER["REQUEST_METHOD"] === "POST") {
	$db_name = "namedb";
	$user = "nameuser";
	$pass = "password";
	$db = new mysqli("database", $user, $pass, $db_name);

	if($db->connect_error) {
		die("Kan niet verbinden met database!");
	}

	$seek = $_POST["seek"];
	$sql = 'SELECT firstname, lastname FROM `names` WHERE `firstname` LIKE "%' . $seek . '%";';
	$result = $db->query($sql);

	if($result->num_rows === 0) {
		die("Niks gevonden.");
	}

	echo "<table><tr><th>Voornaam</th><th>Achternaam</th></tr>";
	while($row = $result->fetch_assoc()) {
		echo "<tr><td>".$row["firstname"]."</td><td>".$row["lastname"]."</td></tr>";
	}
	echo "</table>";

}

?>

</body>


</html>
