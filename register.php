<html>

<head>
    <title>Register to be a human</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="col-md-8 offset-md-2">
    <div class="container">
        <h2>Register to be a human today!</h2>
    </div>
</div>

<?php

if($_SERVER["REQUEST_METHOD"] === "POST") {
    $db_name = "humans";
    $user = "notaspy";
    $pass = "totallynotone";
    $db = new mysqli("database", $user, $pass, $db_name);

    if($db->connect_error) {
        die('<div class="col-md-8 offset-md-2"><div class="content"><p class="text-err">Could not connect to database!</p></div></div>');
    }

    $name = $db->real_escape_string($_POST["name"]);
    $surname = $db->real_escape_string($_POST["surname"]);
    $address = $db->real_escape_string($_POST["address"]);
    $postal_code = str_replace(' ', '', $db->real_escape_string($_POST["postal_code"]));
    $dob = $db->real_escape_string($_POST["dob"]);
    $has_pets = $db->real_escape_string($_POST["pets"] === "on");

    if(! ($name or $surname or $dob) ) {
        die('<div class="col-md-8 offset-md-2"><div class="content"><p>Please fill out all the fields!</p></div></div>');
    }

    if($dob === "0000-00-00") {
        $dob = null;
    }

    if($db->query('SELECT id FROM `general_info` WHERE `name`="'.$name.'" AND `surname`="'.$surname.'";')->num_rows > 0) {
        die('<div class="col-md-8 offset-md-2"><div class="content"><p>Person already in database!</p></div></div>');
    }

    $sql = 'INSERT INTO `general_info` (`name`, `surname`, `address`, `postal_code`, `date_of_birth`, `has_pets`) VALUES ("'.$name.'", "'.$surname.'", "'.$address.'", "'.$postal_code.'", "'.$dob.'", "'.$has_pets.'");';

    if($db->query($sql) === TRUE) {
        echo '<div class="col-md-8 offset-md-2"><div class="content"><p>Successfully registered!</p></div></div>';
    } else {
        echo '<div class="col-md-8 offset-md-2"><div class="content"><p>Something went wrong!</p></div></div>';
    }

}

?>

<div class="col-md-8 offset-md-2" style="margin-top: 5em;">
    <div class="container">
        <form method="post">
            <div class="form-group>
                <label for="name">Name: </label>
                <input class="form-control" id="name" name="name" type="text" required />
            </div>
            <div class="form-group>
                <label for="surname">Surname: </label>
                <input class="form-control" id="surname" name="surname" type="text" required />
            </div>
            <div class="form-group>
                <label for="address">Address: </label>
                <input class="form-control" id="address" name="address" type="text" />
            </div>
            <div class="form-group>
                <label for="postal_code">Postal Code: </label>
                <input class="form-control" id="postal_code" name="postal_code" type="text" />
            </div>
            <div class="form-group>
                <label for="dob">Date of Birth: </label>
                <input class="form-control" id="dob" name="dob" type="date" required />
            </div>
            <div class="form-check" style="padding-top: 1em;">
                <label class="form-check-label">
                    <input class="form-check-input" name="pets" type="checkbox" />
                    I have pets
                </label>
            </div>

            <input class="btn btn-primary float-left" id="submit" name="submit" type="submit" value="Submit" />
            <a class="btn btn-primary float-right" id="view" href="/index.php">View existing humans</a>
        </form>
    </div>
</div>
</body>

</html>
